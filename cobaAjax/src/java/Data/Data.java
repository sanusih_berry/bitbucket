/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
/**
 *
 * @author userr
 */
@ManagedBean
@SessionScoped
public class Data {
    public String name;
    public String message;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    } 
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    public String getMessage() {
        if (name == null) {
            return "Welcome, ";
        }
        else {
            return "Welcome, " + name;
        }
    }
}
