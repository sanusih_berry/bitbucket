/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Employee;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
/**
 *
 * @author userr
 */
@ManagedBean
@SessionScoped
public class UserData {
    private String name;
    private String dept;
    private int age;
    private double salary;
    private ArrayList<Employee> employees = new ArrayList();

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
    
    
    
    public void addEmployee() {
        Employee employee = new Employee(name, dept, age, salary);
        employees.add(employee);
    }
    
    public void deleteEmployee(Employee employee) {
        employees.remove(employee);
    }
}
