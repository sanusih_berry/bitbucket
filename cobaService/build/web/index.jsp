<%-- 
    Document   : index
    Created on : Oct 20, 2015, Oct 20, 2015 10:03:36 AM
    Author     : Berry
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1></h1>
    <%-- start web service invocation --%><hr/>
    <%
    try {
	com.client.HelloWorld_Service service = new com.client.HelloWorld_Service();
	com.client.HelloWorld port = service.getHelloWorldPort();
	 // TODO initialize WS operation arguments here
        java.lang.String name = "berry";
	// TODO process result here
	java.lang.String result = port.hello(name);
	out.println("Result = "+result);
    } catch (Exception ex) {
	// TODO handle custom exceptions here
    }
    %>
    <%-- end web service invocation --%><hr/>
    </body>
</html>
