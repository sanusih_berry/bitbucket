/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Doctor;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import service.DoctorFacade;
import java.util.List;

/**
 *
 * @author Berry
 */
@Named(value = "doctorController")
@SessionScoped
public class DoctorController implements Serializable {

    /**
     * Creates a new instance of DoctorController
     */
    @EJB
    private DoctorFacade df;
    
    private Doctor dr;
    
    public DoctorController() {
        
    }       
    
    public List<Doctor> getAllDoctor() {
        return df.findAll();
    }
    
    public Doctor getDoctor() {
        return dr;
    }
    
    public String prepareCreate() {
        dr = new Doctor();
        return "/AddDoctor";
    }
    
    public String createDoctor() {        
        df.create(dr);      
        return "/index";
    }
    
    
            
}
