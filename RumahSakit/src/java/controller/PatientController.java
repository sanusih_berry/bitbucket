/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Patient;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import service.PatientFacade;
import java.util.List;

/**
 *
 * @author Berry
 */
@Named(value = "patientController")
@SessionScoped
public class PatientController implements Serializable {

    /**
     * Creates a new instance of PatientController
     */
    @EJB
    PatientFacade pf;
    Patient pt;
    
    public PatientController() {
    }
    
    public List<Patient> getAllPatient() {
        return pf.findAll();
    }
}
