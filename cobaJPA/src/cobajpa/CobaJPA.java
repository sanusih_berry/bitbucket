/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobajpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Berry
 */
public class CobaJPA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int i, salary;
        EmployeeService service;
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("cobaJPAPU");
        EntityManager em = emf.createEntityManager();
        
        salary = 1000;
        service = new EmployeeService(em);
        em.getTransaction().begin();
        for (i = 0; i < 10; i++) {
            service.createEmployee(i, "berry" + i, salary);
            salary = salary + 1000;
        }
        em.getTransaction().commit();
        em.close();
        emf.close();
    }
    
}
