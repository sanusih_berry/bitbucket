/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobajpa;

import javax.persistence.*;
import java.util.*;

/**
 *
 * @author Berry
 */
public class EmployeeService {
    EntityManager em;
    
    public EmployeeService(EntityManager em) {
        this.em = em;
    }
    
    public Employee createEmployee(int id, String name, int salary) {
        Employee emp = new Employee(id);
        emp.setNama(name);
        emp.setSalary(salary);
        em.persist(emp);
        return emp;
    }
    
    public Employee findEmployee(int id) {
        return em.find(Employee.class, id);
    }
    
    public void removeEmploye(int id) {
        Employee emp = findEmployee(id);
        em.remove(emp);
    }
}
