/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tebakAngka;

import java.util.Random;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author userr
 */
@ManagedBean
@SessionScoped
public class cobaFacelet {
    int angkaUser;
    int angkaRandom;
    String respon;
    
    public cobaFacelet(){
        Random angka = new Random();
        angkaRandom = angka.nextInt(10);
        System.out.println("Angka Random : " + angkaRandom);
    }
    
    public void setAngkaUser(int angkaUser){
        this.angkaUser = angkaUser;
    }
    
    public int getAngkaUser(){
        return angkaUser;
    }
    
    public String getRespon(){
        if (angkaUser == angkaRandom) {
            return "Tebakan Benar";
        }
        else {
            return "Tebakan Salah, Angka System adalah : " + angkaRandom;
        }
        
    }
}
