/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CalculatorBean;

import javax.ejb.Stateless;

/**
 *
 * @author Berry
 */
@Stateless
public class CalculatorBean implements CalculatorBeanLocal {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public int hitung(int bil1, int bil2) {
        return bil1 + bil2;
    }
}
