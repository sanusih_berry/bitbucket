/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CalculatorBean;

import javax.ejb.Local;

/**
 *
 * @author Berry
 */
@Local
public interface CalculatorBeanLocal {
    public int hitung(int bil1, int bil2);
}
