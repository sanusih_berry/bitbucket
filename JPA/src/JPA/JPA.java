/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JPA;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Berry
 */
public class JPA {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPAPU");
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        Employee emp1 = new Employee();
        emp1.setId(1);
        emp1.setNama("berry");
        emp1.setSalary(1000);
        em.persist(emp1);
        em.getTransaction().commit();
        em.close();
        emf.close();
    }
}
