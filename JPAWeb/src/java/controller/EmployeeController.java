/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.Employee;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import service.EmployeeFacade;
import java.util.List;

/**
 *
 * @author Berry
 */
@Named(value = "employeeController")
@SessionScoped
public class EmployeeController implements Serializable {

    /**
     * Creates a new instance of EmployeeController
     */
    public EmployeeController() {
    }
    
    @EJB
    EmployeeFacade empf;
    
    private Employee employee;
    
    public List<Employee> getAllEmployee() {
        return empf.findAll();                
    }
    
    public Employee getEmployee() {
        return employee;
    }
    
    public String prepareCreate() {
        employee = new Employee();
        return "/AddEmployee";
    }
    
    public String createEmployee() {
        empf.create(employee);
        return "/index";
    }
    
    public String prepareEdit(int id) {
        employee = empf.find(id);
        return "/EditEmployee";
    }
    
    public String editEmployee() {
        empf.edit(employee);
        return "/index";
    }
    
    public String deleteEmployee(int id) {
        employee = empf.find(id);
        empf.remove(employee);
        return "/index";
    }
            
}
